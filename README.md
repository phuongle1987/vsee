## Technical
- [Use KnockoutJS] (http://knockoutjs.com)
- [Use Bootstrap CSS] (http://getbootstrap.com/css/)
- [Use Pubnub JS API] (https://www.pubnub.com/)
- [Use Bitbucket] (https://bitbucket.org)
- [Use Docker Server] (https://www.docker.com/get-started)

## Run app
- Run docker-compose -f docker-compose.yml up -d --build (http://localhost)
- Open provider (http://localhost/provide/index.html)
- Add patient via  (http://localhost/patient/index.html)