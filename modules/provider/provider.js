let Provider = function (providerName) {
  this.providerName = ko.observable(providerName);
  this.patients = ko.observableArray([]);
  this.isShowingPatient = ko.pureComputed(() => {
    const patients = this.patients();
    return Array.isArray(patients) && patients.length > 0;
  }, this);

  const clientUUID = PubNub.generateUUID();
  const theOutGoingChannel = this.providerName();
  const theIncomingChannel = "Patient";

  const pubnub = new PubNub({
    publishKey: "pub-c-17b595c9-4e21-4b82-951f-c09da31e1cf3",
    subscribeKey: "sub-c-da2cbe16-405b-11eb-9ec5-221b84410db5",
    uuid: clientUUID,
  });

  pubnub.subscribe({
    channels: [theIncomingChannel],
    withPresence: true,
  });

  const itemsQuantityToFetch = 100;

  pubnub.history(
    {
      channel: theIncomingChannel,
      count: itemsQuantityToFetch, // how many items to fetch
      stringifiedTimeToken: true, // false is the default
    },
    (status, response) => {
      // handle status, response
      console.log({ status, response });
      if (!status.error && response) {
        this.loadHistory(response.messages);
      }
    }
  );

  this.loadHistory = (historyMessages) => {
    if (!(historyMessages && historyMessages.length > 0)) return;

    const validEntries = historyMessages.reduce((entries, message) => {
      const { entry } = message || {};
      if (entry && entry.patient && entry.type && entry.patient.callId) {
        entries.push(entry);
      }
      return entries;
    }, []);

    const patientsInWaiting = validEntries.reduce((patients, e) => {
      if (e.type === "enter" && !validEntries.some((e1) => e1.type === "exit" && e1.patient.callId === e.patient.callId)) {
        patients.push(e.patient);
      }
      return patients;
    }, []);
    if (patientsInWaiting.length > 0) {
      patientsInWaiting.forEach((p) => {
        this.addPatient(p);
      });
    }
  };

  pubnub.addListener({
    message: (event) => {
      this.onNewMessage(event);
    },
  });

  this.patientOnTheCall = ko.observable(null);

  this.onNewMessage = (event) => {
    const patient = event.message.patient;
    if (event.message.type === "enter") this.addPatient(patient);
    else this.removePatient(patient);

    const patientOnTheCall = this.patientOnTheCall();
    if (patientOnTheCall && event.message.type === "exit" && patient.callId === patientOnTheCall.callId) {
      this.setPatientOnTheCall(null);
    }
  };

  this.addPatient = (patient) => {
    if (!patient) return;
    patient.waitingInMinute = ko.observable(this.getWaitingTime(patient));
    this.patients.push(patient);
  };

  this.removePatient = (exitPatient) => {
    let patients = this.patients();
    if (patients.length <= 0) return;
    patients = patients.filter((p) => p.callId !== exitPatient.callId);
    this.patients(patients);
  };

  this.callPatient = (patient) => {
    this.setPatientOnTheCall(patient);
  };

  this.setPatientOnTheCall = (patient) => {
    const { vseeId, callId } = patient || {};
    this.patientOnTheCall(patient);
    pubnub.publish(
      {
        channel: theOutGoingChannel,
        message: { vseeId, callId },
      },
      (status, response) => {
        if (status.error) {
          console.log(status);
        }
      }
    );
  };

  setInterval(() => {
    const patients = this.patients();
    if (!patients || patients.length <= 0) return;
    const now = new Date();
    patients.forEach((p) => {
      p.waitingInMinute(this.getWaitingTime(p, now));
    });
    this.patients(patients);
  }, 1000);

  this.getWaitingTime = (patient, now = new Date()) => {
    const callDate = new Date(patient.executeTime);
    const diff = now - callDate;
    const MinutesPerDay = 24 * 60 * 60;
    const result = Math.floor(diff / MinutesPerDay);
    return result;
  };
};

ko.applyBindings(new Provider("An Nguyen"));
