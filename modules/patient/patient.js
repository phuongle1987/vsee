let Patient = function (patientName, vseeId, reason = null) {
  this.patientName = ko.observable(patientName);
  this.vseeId = ko.observable(vseeId);
  this.reason = ko.observable(reason);
  this.provider = ko.observable("An Nguyen");
  this.waitingForConnect = ko.observable(false);

  this.getNewUUID = () => {
    return PubNub.generateUUID();
  };

  const uuid = this.getNewUUID();
  const theOutGoingChannel = "Patient";
  const theIncomingChannel = this.provider();

  const callingOther = "Doctor is currently busy and will attend to you soon.";
  const callingYou = "The visit is in progress.";
  const waitMessage = "Your provider will be with your shortly.";

  const pubnub = new PubNub({
    publishKey: "pub-c-17b595c9-4e21-4b82-951f-c09da31e1cf3",
    subscribeKey: "sub-c-da2cbe16-405b-11eb-9ec5-221b84410db5",
    uuid,
  });

  this.enableSubmitButton = ko.pureComputed(() => {
    return this.patientName() && this.vseeId();
  }, this);

  pubnub.subscribe({
    channels: [theIncomingChannel],
    withPresence: true,
  });

  this.callId = ko.observable(null);

  this.setNewCallId = () => {
    this.callId(this.getNewUUID());
  };

  this.enterWaitingRoom = () => {
    if (!this.enableSubmitButton()) return;
    this.waitingForConnect(true);
    this.setNewCallId();
    this.sendPatient("enter");
  };

  this.exitWaitingRoom = () => {
    this.waitingForConnect(false);
    this.sendPatient("exit");
  };

  this.callingMessage = ko.observable(waitMessage);

  this.sendPatient = (action) => {
    pubnub.publish(
      {
        channel: theOutGoingChannel,
        message: { type: action, patient: this.getPatient() },
      },
      (status, response) => {
        if (status.error) {
          console.log(status);
        }
      }
    );
  };

  this.getPatient = () => {
    return {
      fullName: this.patientName(),
      reason: this.reason(),
      vseeId: this.vseeId(),
      executeTime: new Date().toUTCString(),
      callId: this.callId(),
    };
  };

  pubnub.addListener({
    message: (event) => {
      this.onCall(event.message);
    },
  });

  this.onCall = (message) => {
    const { callId } = message;
    if (!callId) {
      this.callingMessage(waitMessage);
      return;
    }

    this.callingMessage(callId === this.callId() ? callingYou : callingOther);
  };
};

ko.applyBindings(new Patient(null, null));
